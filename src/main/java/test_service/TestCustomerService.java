/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author DELL
 */
public class TestCustomerService {
    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        for (Customer customer :cs.getCustomers()) {
            System.out.println(customer);
        }
        //getCustomer
        System.out.println(cs.getByTel("0875512240"));
        //addCustomer
        Customer cus1 = new Customer("noonarak", "0811111111");
        cs.addNew(cus1);
        for (Customer customer :cs.getCustomers()) {
            System.out.println(customer);
        }
        //delCustomer
        Customer delCus = cs.getByTel("0811111111");
        delCus.setTel("0811111112");
        cs.update(delCus);
        System.out.println("After Del");
        for (Customer customer :cs.getCustomers()) {
            System.out.println(customer);
        }
        cs.delete(delCus);
        for (Customer customer :cs.getCustomers()) {
            System.out.println(customer);
        }
    }
}
